package util

type SecuritySettings struct {
	User     string `json:"user"`
	Password string `json:"password"`
}
