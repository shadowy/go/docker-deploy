package util

import (
	"bytes"
	"errors"
	"github.com/docker/docker/api/types/swarm"
	docker "github.com/fsouza/go-dockerclient"
	"github.com/sirupsen/logrus"
	"strings"
)

type DockerHelper interface {
	GetServiceVersion(name string) (*string, error)
	UpdateServiceVersion(name, version string, security *SecuritySettings) (*string, error)
	ExecuteImage(image, version string, env map[string]string, cmd, network *string, security *SecuritySettings) (string, error)
}

type dockerHelper struct {
	client *docker.Client
}

var helper *dockerHelper = nil

func GetDockerHelper() (DockerHelper, error) {
	if helper != nil {
		return helper, nil
	}
	h := &dockerHelper{}
	err := h.init()
	if err != nil {
		return nil, err
	}
	helper = h
	return helper, nil
}

func convertEnvironment(env map[string]string) []string {
	var res []string
	for key, value := range env {
		res = append(res, key+"="+value)
	}
	return res
}

func parseImage(data string) (image, version string) {
	t := strings.Split(data, ":")
	v := strings.Split(t[1], "@")[0]
	return t[0], v
}

func (h *dockerHelper) GetServiceVersion(name string) (*string, error) {
	logrus.WithField("name", name).Debug("dockerHelper.GetServiceVersion")
	service, err := h.getService(name)
	if err != nil {
		return nil, err
	}
	if service == nil {
		return nil, nil
	}
	_, v := parseImage(service.Spec.TaskTemplate.ContainerSpec.Image)
	return &v, nil
}

func (h *dockerHelper) UpdateServiceVersion(name, version string, security *SecuritySettings) (*string, error) {
	logrus.WithFields(logrus.Fields{"name": name, "version": version}).Debug("dockerHelper.UpdateServiceVersion")
	service, err := h.getService(name)
	if err != nil {
		return nil, err
	}
	if service == nil {
		return nil, nil
	}
	image, _ := parseImage(service.Spec.TaskTemplate.ContainerSpec.Image)
	err = h.pullImage(image, version, security)
	if err != nil {
		return nil, err
	}
	err = h.updateImageService(image, version, service)
	if err != nil {
		return nil, err
	}
	return &service.ID, nil
}

func (h *dockerHelper) ExecuteImage(image, version string, env map[string]string, cmd, network *string,
	security *SecuritySettings) (result string, err error) {
	l := logrus.Fields{"image": image, "version": version, "cmd": stringValue(cmd), "network": stringValue(network)}
	logrus.WithFields(l).Debug("dockerHelper.ExecuteImage")
	err = h.pullImage(image, version, security)
	if err != nil {
		return "", err
	}
	container, err := h.client.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Env:   convertEnvironment(env),
			Cmd:   buildCommand(cmd),
			Image: image + ":" + version,
		},
		HostConfig: buildNetwork(network),
	})
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("dockerHelper.ExecuteImage container")
		return "", err
	}
	err = h.client.StartContainer(container.ID, &docker.HostConfig{})
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("dockerHelper.ExecuteImage start")
		return "", err
	}
	status, err := h.client.WaitContainer(container.ID)
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("dockerHelper.ExecuteImage wait")
		return "", err
	}
	l = logrus.Fields{"image": image, "version": version, "status": status}

	outStream := bytes.NewBufferString("")
	errStream := bytes.NewBufferString("")
	err = h.client.Logs(docker.LogsOptions{
		Container:    container.ID,
		OutputStream: outStream,
		ErrorStream:  errStream,
		Stdout:       true,
		Stderr:       true,
	})
	if err != nil {
		logrus.WithFields(l).WithError(err).Error("dockerHelper.ExecuteImage logs")
		return "", err
	}

	result = errStream.String() + "\n output: \n" + outStream.String()
	l = logrus.Fields{"image": image, "version": version, "status": status, "result": result}
	if status != 0 {
		err = errors.New(result)
		logrus.WithFields(l).WithError(err).Error("dockerHelper.ExecuteImage execute")
		return "", err
	}
	return result, nil
}

func (h *dockerHelper) init() error {
	logrus.Info("dockerHelper.init")
	c, err := docker.NewClientFromEnv()
	if err != nil {
		logrus.WithError(err).Error("dockerHelper.init")
		return err
	}
	h.client = c
	return nil
}

func (h *dockerHelper) getService(name string) (*swarm.Service, error) {
	logrus.WithField("name", name).Debug("dockerHelper.getService")
	list, err := h.client.ListServices(docker.ListServicesOptions{})
	if err != nil {
		logrus.WithError(err).WithField("name", name).Error("dockerHelper.getService")
		return nil, err
	}
	for i := range list {
		item := list[i]
		if item.Spec.Name == name {
			return &item, nil
		}
	}
	return nil, nil
}

func (h *dockerHelper) pullImage(image, version string, security *SecuritySettings) error {
	logrus.WithFields(logrus.Fields{"image": image, "version": version}).Debug("dockerHelper.pullService")
	err := h.client.PullImage(
		docker.PullImageOptions{Repository: image + ":" + version},
		buildAuthentication(security),
	)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"image": image, "version": version}).Error("dockerHelper.pullService")
		return err
	}
	return nil
}

func (h *dockerHelper) updateImageService(image, version string, service *swarm.Service) error {
	logrus.WithFields(logrus.Fields{"image": image, "version": version, "service": service.Spec.Name}).
		Debug("dockerHelper.updateImageService")
	service.Spec.TaskTemplate.ContainerSpec.Image = image + ":" + version
	err := h.client.UpdateService(service.ID,
		docker.UpdateServiceOptions{
			Auth: docker.AuthConfiguration{},
			ServiceSpec: swarm.ServiceSpec{
				Annotations:    service.Spec.Annotations,
				TaskTemplate:   service.Spec.TaskTemplate,
				Mode:           service.Spec.Mode,
				UpdateConfig:   service.Spec.UpdateConfig,
				RollbackConfig: service.Spec.RollbackConfig,
				Networks:       service.Spec.Networks,
				EndpointSpec:   service.Spec.EndpointSpec,
			},
			Version: service.Version.Index,
		},
	)
	if err != nil {
		logrus.WithFields(logrus.Fields{"image": image, "version": version, "service": service.Spec.Name}).
			WithError(err).Error("dockerHelper.updateImageService")
		return err
	}
	return nil
}

func stringValue(val *string) string {
	if val == nil {
		return ""
	}
	return *val
}

func buildCommand(cmd *string) []string {
	if cmd == nil {
		return nil
	}
	return []string{*cmd}
}

func buildNetwork(network *string) *docker.HostConfig {
	if network == nil {
		return nil
	}
	return &docker.HostConfig{
		NetworkMode: *network,
	}
}

func buildAuthentication(security *SecuritySettings) docker.AuthConfiguration {
	if security == nil {
		return docker.AuthConfiguration{}
	}
	return docker.AuthConfiguration{
		Username: security.User,
		Password: security.Password,
	}
}
