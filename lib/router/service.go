package router

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/docker-deploy/lib/router/model"
	"gitlab.com/shadowy/go/docker-deploy/lib/util"
	"net/http"
)

type serviceController struct {
	helper util.DockerHelper
}

func initService(root *gin.RouterGroup) error {
	logrus.Info("router.initService")
	router := root.Group("service")
	controller := serviceController{}
	h, err := util.GetDockerHelper()
	if err != nil {
		return err
	}
	controller.helper = h
	router.GET("/:name/version", controller.version)
	router.POST("/:name", controller.update)
	return nil
}

// @Description Get service version
// @Tags Service
// @Security ApiKeyAuth
// @Accept  json
// @Produce  json
// @Param name path string true "Service name with namespace"
// @Success 200 {object} model.ResponseVersion	"Version"
// @Failure 404 {object} model.ResponseError "Service not found"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /service/{name}/version [get]
func (controller serviceController) version(ctx *gin.Context) {
	name := ctx.Param("name")
	logrus.WithField("name", name).Debug("serviceController.version")
	ver, err := controller.helper.GetServiceVersion(name)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	if ver == nil {
		ctx.JSON(http.StatusNotFound, model.ResponseError{Error: "Service '" + name + "' not found"})
		return
	}
	ctx.JSON(http.StatusOK, model.ResponseVersion{Version: *ver})
}

// @Description Set service version
// @Tags Service
// @Security ApiKeyAuth
// @Accept  json
// @Produce  json
// @Param name path string true "Service name with namespace"
// @Param version body model.RequestVersion true "Version"
// @Success 204
// @Failure 404 {object} model.ResponseError "Service not found"
// @Failure 406 {object} model.ResponseError "Data not valid"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /service/{name} [post]
func (controller serviceController) update(ctx *gin.Context) {
	name := ctx.Param("name")
	logrus.WithField("name", name).Debug("serviceController.update")
	var m model.RequestVersion
	if err := ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, model.ResponseError{Error: err.Error()})
		return
	}
	id, err := controller.helper.UpdateServiceVersion(name, m.Version, m.Security)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	if id == nil {
		ctx.JSON(http.StatusNotFound, model.ResponseError{Error: "Service '" + name + "' not found"})
		return
	}
	ctx.Status(http.StatusNoContent)
}
