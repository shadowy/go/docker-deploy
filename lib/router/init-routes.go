package router

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/swaggo/swag/example/celler/httputil"
	"net/http"
)

func InitRoutes(root *gin.RouterGroup, token string) error {
	logrus.Info("router.InitRoutes")
	router := root.Group("/")
	router.Use(auth(token))
	err := initService(router)
	if err != nil {
		return err
	}
	err = initExecutor(router)
	if err != nil {
		return err
	}
	return nil
}

func auth(token string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if ctx.GetHeader("Authorization") == token {
			ctx.Next()
			return
		}
		httputil.NewError(ctx, http.StatusUnauthorized, errors.New("token is not valid"))
		ctx.Abort()
	}
}
