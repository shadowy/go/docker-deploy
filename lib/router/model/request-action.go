package model

import "gitlab.com/shadowy/go/docker-deploy/lib/util"

type RequestAction struct {
	Image       string                 `json:"image"`
	Version     string                 `json:"version"`
	Environment map[string]string      `json:"environment"`
	Command     *string                `json:"command"`
	Network     *string                `json:"network"`
	Security    *util.SecuritySettings `json:"security"`
}
