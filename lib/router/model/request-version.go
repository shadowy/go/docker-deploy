package model

import "gitlab.com/shadowy/go/docker-deploy/lib/util"

type RequestVersion struct {
	Version  string                 `json:"version"`
	Security *util.SecuritySettings `json:"security"`
}
