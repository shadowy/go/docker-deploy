package router

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/docker-deploy/lib/router/model"
	"gitlab.com/shadowy/go/docker-deploy/lib/util"
	"net/http"
)

type executeController struct {
	helper util.DockerHelper
}

func initExecutor(root *gin.RouterGroup) error {
	logrus.Info("router.initExecutor")
	router := root.Group("executor")
	controller := executeController{}
	h, err := util.GetDockerHelper()
	if err != nil {
		return err
	}
	controller.helper = h
	router.POST("/", controller.execute)
	return nil
}

// @Description Execute image
// @Tags Execute
// @Security ApiKeyAuth
// @Accept  json
// @Produce  json
// @Param action body model.RequestAction true "Action"
// @Success 200 body string "Logs"
// @Failure 406 {object} model.ResponseError "Data not valid"
// @Failure 500 {object} model.ResponseError "Internal error"
// @Router /executor [post]
func (controller executeController) execute(ctx *gin.Context) {
	logrus.Debug("executeController.execute")
	var m model.RequestAction
	if err := ctx.BindJSON(&m); err != nil {
		ctx.JSON(http.StatusNotAcceptable, model.ResponseError{Error: err.Error()})
		return
	}
	res, err := controller.helper.ExecuteImage(m.Image, m.Version, m.Environment, m.Command, m.Network, m.Security)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, model.ResponseError{Error: err.Error()})
		return
	}
	ctx.String(http.StatusOK, res)
}
