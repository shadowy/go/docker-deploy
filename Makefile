binaries=docker-deploy
module=email
version :=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1) || echo "v1.0.0"`

all: lint-full

test:
	@echo ">> test"

lint-full: lint card

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

build: $(binaries)

$(binaries):
	@echo ">>build: $@ $(version)"
	@mkdir -p ./dist
	@env CGO_ENABLED=1 go build -ldflags="-X 'gitlab.com/shadowy/sei/$module/$@/lib/settings/version.Version=$(version)'" -o ./dist/$@ ./cmd/$@/main.go
	@chmod 777 ./dist/$@


swagger:
	@echo ">>swagger"
	@swag init -g /cmd/docker-deploy/main.go
