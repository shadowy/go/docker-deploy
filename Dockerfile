FROM golang:alpine as builder
RUN apk add --update make git
ENV GO111MODULE=on
WORKDIR /app
COPY . .
RUN go mod vendor
RUN make build

FROM nginx:alpine
EXPOSE 8080
WORKDIR /app
COPY --from=builder /app/dist/docker-deploy ./
COPY ./docker/start.sh ./
RUN chmod 777 / /app/${command} /app/start.sh
CMD /app/start.sh
