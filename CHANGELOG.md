# CHANGELOG

<!--- next entry here -->

## 0.4.1
2021-03-14

### Fixes

- fix swagger (e8bc45d2708917d1189638c88d7b5fff6946e362)

## 0.4.0
2021-03-07

### Features

- add base-path (c76912eee2145e9128fbe8829d87847a61ceeb12)

## 0.3.1
2021-03-07

### Fixes

- fix docker image (eaef0a2fb213e29ce8eb362ce0d0827c676ebdb3)

## 0.3.0
2021-03-07

### Features

- update docker image building (de42681b75bbb1eb30db227f9e9ad1b7cf4a7f27)

## 0.2.0
2021-03-02

### Features

- add executor (05ab2f514fffd793a181eb4f9f961266419db7c8)

## 0.1.0
2021-02-18

### Features

- initial commit (d1e8521dd3167f765cb032900b7b890dd3cbde92)
- add ci/cd (c1364fb24d68a021fadd0fbd64f0acfeb1719306)

