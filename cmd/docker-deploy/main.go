package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	ginLog "github.com/toorop/gin-logrus"
	"gitlab.com/shadowy/go/docker-deploy/docs"
	"gitlab.com/shadowy/go/docker-deploy/lib/router"
)

var mode = flag.String("mode", "release", "GIN mode (release, debug, test)")
var bind = flag.String("bind", "0.0.0.0:8080", "Bind address and port")
var token = flag.String("token", "", "security token")
var basePath = flag.String("base-path", "/api/", "base path")

// @title Swagger Docker Deploy API
// @version 1.0
// @description This is a Docker Deploy Server
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @BasePath /
func main() {
	logrus.Info("docker-deploy setup")
	flag.Parse()
	logrus.WithFields(logrus.Fields{"mode": *mode, "bind": *bind, "base-path": *basePath}).Info("docker-deploy settings")
	docs.SwaggerInfo.BasePath = *basePath
	gin.SetMode(*mode)
	if *mode == "debug" || *mode == "test" {
		logrus.SetLevel(logrus.DebugLevel)
	}

	server := gin.Default()
	server.Use(ginLog.Logger(logrus.New()), gin.Recovery())
	root := server.Group(*basePath)
	err := router.InitRoutes(root, *token)
	if err != nil {
		logrus.WithError(err).Error("docker-deploy init routes")
		return
	}
	server.GET(*basePath+"swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	logrus.Info("docker-deploy start")
	err = server.Run(*bind)
	if err != nil {
		logrus.WithError(err).Error("docker-deploy")
	}
}
