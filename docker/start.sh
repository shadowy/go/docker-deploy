#!/bin/sh
TOKEN="${TOKEN:-""}"
MODE="${MODE:-"release"}"
BASE_PATH="${BASE_PATH:-"/api/"}"
echo "------------------------------------------------------"
echo "TOKEN=$TOKEN"
echo "MODE=$MODE"
echo "BASE_PATH=$BASE_PATH"
echo "------------------------------------------------------"
/app/docker-deploy --mode=$MODE --token=$TOKEN --base-path=$BASE_PATH
