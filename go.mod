module gitlab.com/shadowy/go/docker-deploy

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/docker/docker v20.10.0-beta1.0.20201113105859-b6bfff2a628f+incompatible
	github.com/fsouza/go-dockerclient v1.7.0
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.5.1
	github.com/toorop/gin-logrus v0.0.0-20200831135515-d2ee50d38dae
)
